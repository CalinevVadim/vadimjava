public class Office18 {

    final static int TOP = 0;
    final static int MIDDLE = 1;
    final static int BOTTOM = 2;

    private static Object[] cabinet = {
            null,
            null,
            null
    };


    
    static void printCabinet() {
        int x = 0;
        for (int num = 0; num < cabinet.length; num++) {
            if (x == 0) {
                System.out.println("######################");
                System.out.printf("# %s               # \n", cabinet[TOP]);
            }
            if (x == 1) {
                System.out.println("######################");
                System.out.printf("# %s               # \n",  cabinet[MIDDLE]);
            }
            if (x == 2) {
                System.out.println("######################");
                System.out.printf("# %s               # \n",  cabinet[BOTTOM]);
            }
            x++;
        }
        System.out.println("######################");
    }



    public static void main(String[] args) {


        cabinet[TOP] = null;


        cabinet[MIDDLE] = "DVD Player";


        cabinet[BOTTOM] = null;

        Office18.printCabinet();
    }
}
