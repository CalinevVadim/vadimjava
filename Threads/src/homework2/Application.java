package homework2;

public class Application {

    public static void main(String[] args) {
        BreadProvider provider = new BreadProvider();
        BreadConsumer consumerA = new BreadConsumer("John",provider);
        BreadConsumer consumerB = new BreadConsumer("Marry",provider);

        provider.start();
        consumerA.start();
        consumerB.start();
        /*
        Метод покупки не синхронизирован, поэтому бывает, что два консумера покупает один хлеб. (мы это исправили)
         "public synchronized Bread sellOneBread()"
         В остальном все в порядке. Идет процесс: производство -> продажа -> потребление (по кругу)*/
    }

}