package homework1;

public class CounterRacingSimple {

    public static void main(String[] args) {
        // 1)Нам не нужно хранить переменную "counter" для дальнейших операций. Нам нужен всего один экземпляр для операции.
        new CounterRacingSimple().doCounter();


    }

    private int count = 0;

    private synchronized void increment() {
        count++;
    }

    private void doCounter() {
        // 2)Внутри конструктора Thread мы реализуем интерфейс Runnable
        Thread t1 = new Thread(new Runnable() {

            @Override
            public void run() {
                // 3)Миллион для каждого
                for(int i = 0; i < 1_000_000; i++) {
                    increment();

                }
                System.out.println("t1: " + count);

            }
        });

        Thread t2 = new Thread(new Runnable() {

            @Override
            public void run() {
                for(int i = 0; i < 1_000_000; i++) {
                    increment();
                }
                System.out.println("t2: " + count);

            }
        });
        // 4) певрое число означает, что один из потоков закончил свою работу(досчитал до миллиона) а другой нет и вывел на экран
        // результат общей переменнной
        //второе число - когда последний поток досчитал свое число
        //они меняються местами, потому что у них динамичная скорость потока (то один первый, то другой)

        t1.start();
        t2.start();
    }

}