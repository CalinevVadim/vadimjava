package humanity;

public class Person {



    private String name;
    private Person friend;

    public String getName() {
        return name;
    }

    public Person(String name){
       this.name = name;
    }
    // ???
    public boolean addFriend(Person friend) {
        if (friend == null || this.name.equals(friend.getName()) ) {
        return false;
        } else {
            this.friend = friend;
            return true;
        }
    }
    public boolean removeFriend(){
        if ( friend == null){
            return false;
        } else {
            this.friend = null;
            return true;
        }
    }

    public boolean isLonely(){
        if ( friend == null){
            return true;
        } else {
            return false;
        }
    }

    String getFriendName() {
        if (friend == null) {
            return null;
        } else {
            return friend.getName();
        }
    }
}
