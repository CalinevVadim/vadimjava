package objectsAndMethods;

public class Converter {
    public static void main(String[] args) {
        System.out.println(convertEurToUsd(10 , 0.92));
        System.out.println(convertEurToUsd(100 ,0.97));

    }

    public static double convertEurToUsd(int eur, double exchangeRate) {
        double result = 0;
        result = eur*exchangeRate;
        return result;

    }
}
