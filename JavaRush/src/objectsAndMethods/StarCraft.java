package objectsAndMethods;

public class StarCraft {
    public static void main(String[] args) {

        Zerg one = new Zerg();
        one.name = "One";
        Zerg two = new Zerg ();
        two.name = "Two";
        Zerg three = new Zerg ();
        three.name = "Three";
        Zerg four = new Zerg ();
        four.name = "Four";
        Zerg five = new Zerg ();
        five.name = "Five";

        Protoss oneP = new Protoss ();
        oneP.name = "One";
        Protoss twoP = new Protoss ();
        twoP.name = "Two";
        Protoss threeP = new Protoss ();
        threeP.name = "Three";

        Terran oneT = new  Terran ();
        oneT.name = "Two";
        Terran twoT = new  Terran ();
        twoT.name = "Three";
        Terran threeT = new  Terran ();
        threeT.name = "Four";
        Terran fourT = new  Terran ();
        four.name = "Five";


    }

    public static class Zerg {
        public String name;
    }

    public static class Protoss {
        public String name;
    }

    public static class Terran {
        public String name;
    }
}
