public class Artifact {
    int idNumber;
    String culture;
    int century;

    public Artifact(int idNumber) {
        this.idNumber = idNumber;
    }



    public Artifact(int idNumber, String culture) {
        this.idNumber = idNumber;
        this.culture = culture;
    }

    public Artifact(int idNumber, String culture, int century) {
        this.idNumber = idNumber;
        this.culture = culture;
        this.century = century;
    }

    public static void main(String[] args) {
            Artifact only = new Artifact(2121);
            Artifact more = new Artifact(2122, "Aztec");
            Artifact all = new Artifact(2123, "Aztec", 15);
        System.out.println(only.idNumber);
        System.out.println(more.idNumber + " " + more.culture);
        System.out.println(all.idNumber + " " + all.culture + " " + all.century);
    }
}