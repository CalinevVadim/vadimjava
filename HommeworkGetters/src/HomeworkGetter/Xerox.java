package HomeworkGetter;

import java.util.ArrayList;
import java.util.List;

public class Xerox {

    public static PaperDocumentInterface copy(PaperDocumentInterface original){
        return new PaperDocument(original.getType(),original.getTitle(),original.getContent()) ;

    }
    public static List<PaperDocumentInterface> copy(PaperDocumentInterface original, int quantity){
        ArrayList<PaperDocumentInterface> docArray = new ArrayList<>(quantity);
        docArray.add(new PaperDocument(original.getType(),original.getTitle(),original.getContent()));
        return docArray;
    }

}