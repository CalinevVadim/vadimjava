package main;

import HomeworkGetter.PaperDocument;
import HomeworkGetter.PaperDocumentInterface;
import HomeworkGetter.Xerox;

public class Application {
    public static void main(String[] args) {


        PaperDocument doc = new PaperDocument("Contract", " Java Developer Hiring Contract",
                "1. Working hours per day: 6\n2. Earn per hour: 15EUR\n3. Hollidays per year: 30days.");
        PaperDocumentInterface doc2 = Xerox.copy(doc);
        doc2.setType("Contract Copy");
        PaperDocumentInterface doc3 = Xerox.copy(doc);
        doc3.setType("Archived Contract Copy");
        PaperDocumentInterface doc4 = Xerox.copy(doc);
        doc4.setType("Archived Contract Copy");
        PaperDocumentInterface doc5 = Xerox.copy(doc);
        doc5.setType("Archived Contract Copy");
        System.out.println(doc2);
        System.out.println(doc3);
        System.out.println(doc4);
        System.out.println(doc5);

    }
}
