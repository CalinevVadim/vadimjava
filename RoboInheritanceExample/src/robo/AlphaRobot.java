package robo;

public class AlphaRobot extends AbstractRobot {
    private final byte MAX_VALUE = 100;
    private final byte MIN_VALUE = 0;
    private String name;
    private String model;
    private int x;
    private int y;

    AlphaRobot(String name, String model) {
        setName(name);
        setModel(model);
        x = 0;
        y = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {

        } else
            this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        if (model == null) {

        } else
            this.model = model;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        if (x < MIN_VALUE || x > MAX_VALUE ) {

        } else
            this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        if (y < MIN_VALUE || y > MAX_VALUE ) {

        }else
            this.y = y;
    }

    public boolean moveRight() {
        if (hasRight())
            setX(++x);
        return x < MAX_VALUE ;
    }

    public boolean moveLeft() {
        if (hasLeft())
            setX(--x);
        return x > MIN_VALUE;
    }

    public boolean moveDown() {
        if (hasDown())
            setY(++y);
        return y < MAX_VALUE ;
    }

    public boolean moveUp() {
        if (hasUp())
            setY(--y);
        return y > MIN_VALUE;
    }

    boolean hasRight() {
        return x + 1 < MAX_VALUE ;
    }

    boolean hasLeft() {
        return x - 1 > MIN_VALUE;
    }

    boolean hasUp() {
        return y - 1 > MIN_VALUE;
    }

    boolean hasDown() {
        return y + 1 < MAX_VALUE ;
    }



}