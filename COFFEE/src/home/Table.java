package home;

public class Table {
    Cup cup;

    public Table(Cup cup){
      this.cup = cup;
    }

    public Cup getCup() {
        return cup;
    }
}
