package ingredients;

public class Water {
    int gramms; // weight in gramms

    public void setGramms(int gramms) {
        this.gramms = gramms;
    }

    public int getGramms() {
        return gramms;
    }
}
