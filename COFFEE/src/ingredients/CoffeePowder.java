package ingredients;

public class CoffeePowder {
    int gramms; // weight in gramms

    public void setGramms(int gramms) {
        this.gramms = gramms;
    }

    public int getGramms() {
        return gramms;
    }
}
