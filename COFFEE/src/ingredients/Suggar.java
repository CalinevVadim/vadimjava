package ingredients;

public class Suggar {
    int milliliters; // volume in milliliters

    public void setMilliliters(int milliliters) {
        this.milliliters = milliliters;
    }

    public int getMilliliters() {
        return milliliters;
    }
}
