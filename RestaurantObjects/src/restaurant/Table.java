package restaurant;
import world.Person;
public class Table {
    // properties
    int number;
    Person seatA;
    Person seatB;

    // constructor
    public Table(int number) {
        this.number = number;
    }

    public void setNumber(int x) {
        number = 0;
        if (x < 1 || x > 3) {
            System.err.println("Error! We have just 3 tables! -");
        } else {
            number  =  x;
        }

    }
   public void setSeat(Person person, char AorB) {
        if (AorB == 'A') {
            seatA = person;
        } else if (AorB == 'B') {
            seatB = person;
        }
    }



public boolean isFree(){
        if( seatA == null && seatB == null){
            return true;
        } else {
            return false;
        }

    }
  public Person getSeat(char x ){
      if (x == 'A') {
        return  seatA ;
      } else if (x == 'B') {
       return  seatB ;
      } else {
          return null;
      }
  }

}
