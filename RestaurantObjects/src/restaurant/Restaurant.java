package restaurant;

import world.Person;

public class Restaurant {
    // properties
    private String name;
    private Table tableNearWindow;
    private Table tableNearDoors;
    private Table tableNearBathroom;
    Person waiter;

    public void setTable(Table table, int x) {
        if (x == 1) {
            tableNearWindow = table;
        } else if (x == 2) {
            tableNearDoors = table;
        } else if (x == 3) {
            tableNearBathroom = table;
        } else {
            System.err.println("Error!");
        }
    }

    public boolean isFree() {
        if (tableNearWindow == null && tableNearDoors == null && tableNearBathroom == null) {
            return true;
        } else {
            return false;
        }
    }

    public Person getWaiter() {
        return waiter;
    }

    public void setWaiter(Person waiter) {
        this.waiter = waiter;
    }

    // constructor
    public Restaurant(String name) {
        this.name = name;
    }

    public void printInfo() {
        System.out.println("RESTAURANT: Good Morning Sunshine!");
        waiter.printInfo();
        System.out.println("##################################");
        System.out.println("----------------------------------");
        System.out.println("Table 1 :");
        tableNearWindow.getSeat('A').printInfo();
        tableNearWindow.getSeat('B').printInfo();
        System.out.println("Table 2 :");
        tableNearDoors.getSeat('A').printInfo();
        tableNearDoors.getSeat('B').printInfo();
    }
}


