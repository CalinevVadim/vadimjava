package world;

public class Person {
    // properties
    private String fullName;
    private char gender;
    private int age;
    private String jobTitle;

    public void setGender(char x) {
        gender = 0;
        if (x != 'm' || x != 'f') {
            System.err.println("Error!");
        } else {
            gender = x;
        }

    }

    public void setAge(int x) {
        age = 0;
        if (x < 13 || x > 250) {
            System.err.println("Error!");
        } else {
            age = x;
        }

    }


    // constructor
    public Person(String fullName, char gender, int age) {
        this.fullName = fullName;
        if (gender != 'm' && gender != 'f') {
            System.err.println("Error!");
        } else {
            this.gender = gender;
        }

        if (age < 13 || age > 250) {
            System.err.println("Error!");
        } else {
            this.age = age;
        }
    }

    public void setJobTitle(String x){
        jobTitle = x;
    }

    public String  getJobTitle(){
      return  jobTitle ;
    }

    public void printInfo() {
        System.out.printf("%s (%s) : %s , %d years \n", fullName, jobTitle, gender, age);
    }
}