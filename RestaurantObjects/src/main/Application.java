package main;

import restaurant.Restaurant;
import restaurant.Table;
import world.Person;

public class Application {
    public static void main(String[] args) {
        Person person1 = new Person("Vadim", 'm', 23);
        person1.setJobTitle("Designer");
        Person person2 = new Person("Vadim2", 'm', 23);
        person2.setJobTitle("Medic");
        Person person3 = new Person("Vadim3", 'f', 25);
        person3.setJobTitle("Soldier");
        Person person4 = new Person("Vadim4", 'f', 24);
        person4.setJobTitle("Teacher");

        Person waiter = new Person("Vasea", 'm', 36);
        waiter.setJobTitle("Waiter");

        Table table = new Table(1);
        table.setSeat(person1, 'A');
        table.setSeat(person2, 'B');
        Table table2 = new Table(2);
        table2.setSeat(person3, 'A');
        table2.setSeat(person4, 'B');

        Restaurant restaurant = new Restaurant("Kukish");
        restaurant.setWaiter(waiter);
        restaurant.setTable(table, 1);
        restaurant.setTable(table2, 2);
        restaurant.printInfo();


    }


}
