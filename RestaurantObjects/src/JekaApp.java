public class JekaApp{
    public static void main(String[] args) {
        Car car = new Car();
        car.setModel("BMW");
        car.setNumber("A.H. - 1488");
        Parking parking = new Parking();
        parking.parkCar(car, "1A");
        parking.printMap();
    }

}
