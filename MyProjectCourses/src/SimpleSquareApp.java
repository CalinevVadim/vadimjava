import java.util.Scanner;
public class SimpleSquareApp {
    public static void main (String[]args){
        final int SIZE = 10;
        int lCode = 3;
        int sCode = 8;
        for (int l = 1; l <= SIZE; l++ ){
            for (int s = 1; s <= SIZE; s++ ){
                if (l == 1 || l == SIZE ||  s == 1) {
                    System.out.print("\u20ac ");
                } else {
                    if (s == sCode && l == lCode ) {
                        System.out.print("x");}
                    else {
                        System.out.print(" ");}
                    }
                }
            System.out.println();
            }
        }
    }

