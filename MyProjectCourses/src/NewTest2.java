
public class NewTest2 {
    public void main(String[] args) {
        final int SIZE = 7;
        int lCoord = 4;
        int sCoord = 4;
        for (int l = 1; l <= SIZE; l++) {
            for (int s = 1; s <= SIZE; s++) {
                if (l == 1 || l == SIZE || s == 1 || s == SIZE) {
                    System.out.print("# ");
                } else {
                    if (l == lCoord && s == sCoord) {
                        System.out.print("x ");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }
}
