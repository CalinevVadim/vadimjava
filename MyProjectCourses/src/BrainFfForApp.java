public class BrainFfForApp {
    public static void main(String[] args) {

        int a=1,b=1,c=1;// инициализируем переменные a,b,c | присваеваем им значение "1".

        for( b=a++; c<++b; a = c+=2 ) { /* b = (a(1) + 1) = 1 так как "а" увеличилось уже после вычисления|
        c < b (2 так как мы вначале прибавили "a") | "a" == 3 , но! так как "с" теперь равен 3, то это больше "b".
        Операция выполнится один раз, после чего уже не будет действовать. Если бы мы продолжили ее и вывели снова то ответ был бы уже другой.
        */
            System.out.printf("%d %d %d\n",--a,--b, c);/* c==1 (так как не менялось) b==1 (так как от 2 отняли 1)
           a == 1, так как от 2 отняли 1 (a = c+=2 выполниться уже в след раз)
        */
        }

    }
}
