import java.util.Scanner;
public class DrawCakeWithForApp {

    public static void main(String[] args) {
        final int START = 1;
        final int SMOKE = 2;
        final int FIRE = 3;
        final int CANDLE = 4;
        final int CREAM = 5;
        final int BASE = 6;
        final int FINISH = 7;
        Scanner sc = new Scanner (System.in);
        System.out.println("Enter cakes width");
        int width = sc.nextInt();
        System.out.println("Enter number of candles");
        int candleWidth = sc.nextInt();



        for (int level = 1; level < 8; level++) {
            switch (level) {
                case START: System.out.print(",,,");
                System.out.println();
                break;
                case SMOKE: for (int lines = 1; lines <= 2; lines++) {
                    for (int element = 1; element <= candleWidth; element++) {
                        System.out.print(" .");
                    }
                    System.out.println();
                }
                    break;
                    case FIRE:
                        for (int element = 1; element <= candleWidth; element++) {
                            System.out.print(" ^");
                        }
                        System.out.println();

                    break;
                case CANDLE: for (int element = 1; element <= candleWidth; element++){
                    System.out.print(" |");
                }
                    System.out.println();
                    break;
                case CREAM: for (int element = 1; element <= width; element++){
                        System.out.print("~");
                    }
                        System.out.println();
                    break;
                case BASE:
                    for (int lines = 1; lines <= 3; lines++) {
                        for(int elements=1; elements<= width; elements++) {
                            System.out.print("#");
                        }
                        System.out.println();
                    }
                    break;
                case FINISH: System.out.print(",,,");
                    System.out.println();
                    break;
            }
        }
    }
}
