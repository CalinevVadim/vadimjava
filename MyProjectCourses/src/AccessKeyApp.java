import  java.util.Scanner;
public class AccessKeyApp {
    public static void main(String[] args) {

        final String ACCESS_KEY = "0xFEDCBA9876543210";// формат - long;
        String key;
        Scanner in = new Scanner(System.in);
        System.out.println( "Enter key");
         key = in.nextLine();
         if (ACCESS_KEY.equals(key)) {
             System.out.println("Access Granted!");
         }
         else {
             System.out.println( "Access Denied!");
         }

    }
}
