

// main class
public class DataWrapApp {
    public static void main(String[] args) {
System.out.println(DataWrapper.wrapSquare("array"));
System.out.println(DataWrapper.wrapAngle("tag"));
System.out.println(DataWrapper.wrapRound(5));
    }
}

// secondary class
class DataWrapper {
    static String wrapRound( String text ) {
        return "(" + text + ")";
    }
    static String wrapSquare( String text ) {
        return "[" + text + "]";
    }
    static String wrapAngle( String text ) {
        return "<" + text + ">";
    }

    static String wrapRound(int x ) {
        return "(" + x + ")";
    }
    static String wrapSquare( int x ) {
        return "[" + x + "]";
    }

    static String wrapAngle( int x ) {

        return "<" + x + ">";
    }

    static String wrapRound( double x ) {
        return "(" + x + ")";
    }

    static String wrapSquare( double x ) {
        return "[" + x + "]";
    }
    static String wrapAngle( double x ) {
        return "<" + x + ">";
    }
    static String wrapRound(char x ) {
        return "(" + x + ")";
    }
    static String wrapSquare( char x ) {
        return "[" + x + "]";
    }
    static String wrapAngle( char x ) {
        return "<" + x + ">";
    }
}