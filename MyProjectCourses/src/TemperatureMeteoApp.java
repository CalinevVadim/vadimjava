import  java.util.Random;
public class TemperatureMeteoApp {
    public static void main(String[] args) {
        PrivateMeteoProvider.getCurrentTemperature(840);
        PrivateMeteoProvider.getCurrentHumidity(841);
    }
}

class OpenMeteoProvider {
    static int getCurrentTemperature(int countryCode) {
       int code = countryCode;
       int min = -50;
       int max = 50;
        int diff = max - min;
        Random random = new Random();
        int i = random.nextInt(diff + 1);
        i += min;
        switch (code){
            case 840:
                 System.out.println("In USA now is " + i+ " %C");
                break;
            case 804:
                System.out.println("In Ukraine now is " + i+ " %C");
                break;
            case 688:
                System.out.println("In Serbia now is " + i+ " %C");
                break;
            case 642:
                System.out.println("In Romania now is " + i + " %C");
                break;
            case 643:
                System.out.println("In Russia now is " + i + " %C");
                break;
            default:
                System.err.println( "Can't provide data for this country!");
        }
        return i;
    }

}

class PrivateMeteoProvider extends OpenMeteoProvider {

    static int getCurrentHumidity(int countryCode) {
        int code = countryCode;
        int i = 0;
        if (code == 840 || code == 804 || code == 688 || code == 642 || code == 643) {
            int min = 0;
            int max = 100;
            int diff = max - min;
            Random random = new Random();
            i = random.nextInt(diff + 1);
            i += min;
            System.out.println("Влажность воздуха " + i + " %");
            return i;
        } else {
            System.err.println("Влажность воздуха в этом регионе не известна");
            return i;
        }
    }
}