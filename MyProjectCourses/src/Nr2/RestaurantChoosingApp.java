package Nr2;

import java.io.PrintStream;

public class RestaurantChoosingApp  {
    public static void main(String[] args) {
        SushiRestaurant.Info.printMenu();
        VeganRestaurant.Info.printMenu();


    }
}

// ###################### RESTAURANT CLASSES ##########################
class SushiRestaurant {
    static class Info extends SushiRestaurant  {
        static void printName() {
            System.out.println("########### Sushi Mini Restaurant ###########");
        }

        static void printMenu() {
           printName();
           printAddress();
           Menu.getPrice();
           int numberOfPortions = 1;
           System.out.printf("MENU         x %d portions       %3.2f  MDL \n", numberOfPortions , Menu.getPrice(numberOfPortions));
            numberOfPortions = 5;
            String discount = "(-10%)";
            System.out.printf("MENU         x %d portions%s %3.2f  MDL \n" , numberOfPortions, discount ,  Menu.getPrice(numberOfPortions) );
            numberOfPortions = 1;
            System.out.printf("SPECIAL MENU x %d portions       %3.2f  MDL \n", numberOfPortions , SpecialMenu.getPrice(numberOfPortions) );
            numberOfPortions = 5;
           discount = "(-20%)";
            System.out.printf("SPECIAL MENU x %d portions%s %3.2f MDL \n" , numberOfPortions, discount , SpecialMenu.getPrice(numberOfPortions) );
        }
        static void printAddress() {
            System.out.println("-- DownTown Str 88/A, Minicity             --");
        }
    }

    static class Menu {
        static double getPrice() {
            return 100.0;
        }

        static double getPrice(int portions) {
            int result = 0;
            int discount = ((portions * 100) / 100 * 10);
            if (portions < 5) {
                result = portions * 100;
            } else {
                result = ((portions * 100) - discount);}
            return result;
        }
        }

    static class SpecialMenu {

        static double getPrice() {
            return 300.0;
        }
        static double getPrice(int portions) {
            int result = 0;
            int discount = ((portions * 300)/100 * 20);
            if (portions < 10) {
                result = portions * 300;
            } else {
                result = ((portions * 300) - discount);}
            return result;
        }

    }
}

class  VeganRestaurant {
    static class Info extends VeganRestaurant {
        static void printName() {
            System.out.println("########### Vegan Pure Restaurant ###########");
        }
        static void printMenu() {
            printName();
            printAddress();
            Menu.getPrice();
            int numberOfPortions = 1;
            System.out.printf("MENU         x %d portions        %3.2f   MDL \n", numberOfPortions , Menu.getPrice(numberOfPortions));
            numberOfPortions = 5;
            String discount = "(-5%)";
            System.out.printf("MENU         x %d portions%s   %3.2f  MDL \n" , numberOfPortions, discount ,  Menu.getPrice(numberOfPortions) );
            numberOfPortions = 1;
            System.out.printf("SPECIAL MENU x %d portions        %3.2f  MDL \n", numberOfPortions , SpecialMenu.getPrice(numberOfPortions) );
            numberOfPortions = 10;
            discount = "(-10%)";
            System.out.printf("SPECIAL MENU x %d portions%s %3.2f MDL \n" , numberOfPortions, discount , SpecialMenu.getPrice(numberOfPortions) );
        }
        static void printAddress() {
            System.out.println("-- UpTown Str 77/B, Maxicity               --");
        }
    }
    static class Menu {
        static double getPrice() {
            return 50.0;
        }
        static double getPrice(int portions) {
            double result = 0;
            double discount = ((portions * 50) / 100 * 5);
            if (portions < 5) {
                result = portions * 50;
            } else {
                result = ((double)((portions * 50) - discount));}
            return (double) result;
        }
        }

    static class SpecialMenu {
        static void printMenu() {
            System.out.println("SPECIAL MENU");
            System.out.println("SPECIAL MENU");
        }

        static double getPrice() {
            return 200.0;
        }
        static double getPrice(int portions) {
            double result = 0;
            int discount = ((portions * 200) / 100 *10);
            if (portions < 10) {
                result = portions * 200;
            } else {
                result = ((portions * 200) - discount);}
            return result;
        }
    }
}


