import java.util.Scanner;
public class DrawLineWithForApp {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);


        System.out.println("Enter line length: ");

        byte length = sc.nextByte();

        System.out.println("Enter line direction: ");

        char direction = sc.next().charAt(0);
        switch (direction) {
            case 'h':
                for (int i = 1; i <= length; i++) {
                    System.out.print("-");
                } if (length <= 0) {
                    System.out.println("Error! Length can't be lesser than 1");
            }
                break;
            case 'v':
                for (int i = 1; i <= length; i++) {
                    System.out.println("|");
                } if (length <= 0) {
                System.out.println("Error! Length can't be lesser than 1");
            }
                break;
            default:
                System.out.println("Error! Check entered data");
                break;
        }
    }
}
