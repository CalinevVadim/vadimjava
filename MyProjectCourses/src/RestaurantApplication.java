import java.util.Scanner;

public class RestaurantApplication {
    public static void main(String[] args) {
        //DATA LAYER
        final String CURRENCY = "MDL";
        final String FOOD_1_NAME = "\"Pizza\"";
        int food_1_available_quantity = 5;
        final float FOOD_1_PRICE = 75.5f;

        final String FOOD_2_NAME = "\"Mamaliga\"";
        int food_2_available_quantity = 5;
        final float FOOD_2_PRICE = 150f;

        final String FOOD_3_NAME = "\"Salad\"";
        int food_3_available_quantity = 5;
        final float FOOD_3_PRICE = 30f;

        //PRESENTATION and LOGIC LAYER

        System.out.printf("\n **********MENU**********\n" +
                        "1. %-10s %7.2f %s \n" +
                        "2. %-10s %7.2f %s \n" +
                        "3. %-10s %7.2f %s \n" +
                        " ***********************\n",
                FOOD_1_NAME, FOOD_1_PRICE, CURRENCY,
                FOOD_2_NAME, FOOD_2_PRICE, CURRENCY,
                FOOD_3_NAME, FOOD_3_PRICE, CURRENCY);

        Scanner sc = new Scanner(System.in);
        System.out.println(" CHOISE AN OPTION >>>");

        if (sc.hasNextInt()) {
            int option = sc.nextInt();
            String nameOfOrder = null;
            int numberInRestaurant = 0;
            if (option >= 1 && option <= 3) {
                if (option == 1) {
                    System.out.printf("Your choise is: %s\n", FOOD_1_NAME);
                    nameOfOrder = FOOD_1_NAME;
                    numberInRestaurant  = food_1_available_quantity;
                }else if (option == 2) {
                    System.out.printf("Your choise is: %s\n", FOOD_2_NAME);
                    nameOfOrder = FOOD_2_NAME;
                    numberInRestaurant  = food_2_available_quantity;
                } else if (option == 3) {
                    System.out.printf("Your choise is: %s\n", FOOD_3_NAME);
                    nameOfOrder = FOOD_3_NAME;
                    numberInRestaurant = food_3_available_quantity;
                }
                System.out.println("How many do you want ?");
                if (sc.hasNextInt()) {
                    int quantity = sc.nextInt();
                    int answer = 0;
                    float sum = 0;
                    if (quantity > 0) {
                        if (option == 1 && quantity <= food_1_available_quantity) {
                            sum = quantity * FOOD_1_PRICE;
                            System.out.printf("%d x %s (%5.2f) = %4.2f %s\n" +
                                            "Confirm order( 1 - Yes , 0 - No)?\n",
                                    quantity, FOOD_1_NAME, FOOD_1_PRICE, sum,CURRENCY);
                            numberInRestaurant  = food_1_available_quantity;
                            answer = sc.nextInt();
                        }
                        else
                        if (option == 2 && quantity <= food_2_available_quantity) {
                            sum = quantity * FOOD_2_PRICE;
                            System.out.printf("%d x %s (%5.2f) = %4.2f %s\n" +
                                            "Confirm order( 1 - Yes , 0 - No)?\n",
                                    quantity, FOOD_2_NAME, FOOD_2_PRICE, sum,CURRENCY);
                            numberInRestaurant  = food_2_available_quantity;
                            answer = sc.nextInt();
                        } else
                        if (option == 3 && quantity <= food_3_available_quantity) {
                            sum = quantity * FOOD_3_PRICE;
                            System.out.printf("%d x %s (%5.2f) = %4.2f %s\n" +
                                            "Confirm order( 1 - Yes , 0 - No)?\n",
                                    quantity, FOOD_3_NAME, FOOD_3_PRICE, sum,CURRENCY);
                            numberInRestaurant  = food_3_available_quantity;
                            answer = sc.nextInt();
                        }else {

                            System.out.printf("You'v ordered too many %s , we only have %d\n" +
                                    "PLEASE TRY AGAIN!", nameOfOrder , numberInRestaurant );
                        }
                        if (answer == 1) {
                            System.out.println("THANK YOU!");
                            System.out.println("Do you want delivery?\n" +
                                    "( 1 - YES , 0 - NO )");
                            answer = sc.nextInt();
                            System.out.println("OK");
                            if (answer == 1 && sum < 200) {
                                System.out.printf("Delivery will be + %.2f\n", 50f);
                            }
                        }
                    }
                } else {
                    System.out.println("You have not introduced a number");
                }
            }else {
                System.out.println("The introduced number is not in Menu");
            }
        }else {
            System.out.println("You introduced not a number");
        }
    }
}


