public class ObjectMap {

    public static void main(String[] args) {
        printMap(10, 10, 5, 5, 3, 3);
        printMapArea(10, 10);
        printViewDirection(5, 5, 3, 3);
    }

    private static void printViewDirection(int x, int y, int cx, int cy) {
        if(x > cx && y == cy) System.out.println("The Object is at N of point");
        else if(x < cx && y == cy) System.out.println("The Object is at S of point");
        else if(y > cy && x == cx) System.out.println("The Object is at W of point");
        else if(y < cy && x == cx) System.out.println("The Object is at E of point");
        else if(y > cy && x > cx) System.out.println("The Object is at NW of point");
        else if(y > cy && x < cx) System.out.println("The Object is at NE of point");
        else if(y < cy && x < cx) System.out.println("The Object is at SW of point");
        else if(y < cy && x > cx) System.out.println("The Object is at SE of point");
    }

    private static void printMapArea(int hsize, int vsize) {
        System.out.println("The zone area is " + hsize * vsize);
    }

    static void printMap(int hsize, int vsize, int x, int y, int cx, int cy){
        hsize += 4;
        vsize += 4;
        x += 2;
        y += 2;
        cx += 2;
        cy += 2;

        for(int i1 = 0; i1 <= hsize; i1++){

            for(int i2 = 0; i2 <= vsize; i2++){
                if(i1 == x && i2 == y) System.out.print("0 ");
                else if(i1 == cx && i2 == cy) System.out.print("C ");

                else if(i2 ==hsize/2 && i1 == 0) System.out.print("^");
                else if(i2 == hsize/2 && i1 == 1) System.out.print("N ");
                else if(i2 ==hsize/2 && i1 == hsize) System.out.print("v");
                else if(i2 ==hsize/2 && i1 == hsize-1) System.out.print("S ");
                else if(i1 ==vsize/2 && i2 == 0) System.out.print("W ");
                else if(i1 ==vsize/2 && i2 == 1) System.out.print("< ");
                else if(i1 ==vsize/2 && i2 == vsize) System.out.print("E ");
                else if(i1 ==vsize/2 && i2 == vsize-1) System.out.print("> ");

                else if(i1 > hsize-2 || i1 < 2) System.out.print("  ");
                else if(i2 > vsize-2 || i2 < 2) System.out.print("  ");
                else if(i1 == 2 && i2 > 2) System.out.print((i2-2)+ " ");
                else if(i2 == 2 && i1 > 2){
                    if(i1 > 11) System.out.print(i1-2);
                    else System.out.print(i1-2 + " ");
                }
                else System.out.print(". ");
            }

            System.out.println();
        }
    }
}