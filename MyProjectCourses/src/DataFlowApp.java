// main class
public class DataFlowApp {
    public static void main(String[] args) {
System.out.println(DataTransformer.integerToByte(1000));
        System.out.println(DataTransformer.byteToInteger((byte) 100));
        System.out.println(DataTransformer.doubleToInteger(1000));
        System.out.println(DataTransformer.integerToDouble(1000));
        System.out.println(DataTransformer.shortToInteger((short) 100));
        System.out.println(DataTransformer.integerToShort(1000));
    }
}

// secondary class
class DataTransformer {
    static byte integerToByte(int value) {
        byte result = 0;
        if (value > 127 || value < -128) {
            System.err.println("Error! Out of value! - ");
        } else {
            result = (byte) (value);
        }
        return result;
    }
    static int byteToInteger(byte value) {
        int result = 0;
        if (value > 2147483647 || value < -2147483648) {
            System.err.println("Error! Out of value! - ");
        } else {
            result = (int) (value);
        }
        return result;
    }

    static int doubleToInteger(double value) {
        int result = 0;
        if (value > 2147483647 || value <  -2147483648) {
            System.err.println("Error! Out of value! - ");
        } else {
            result = (int) (value);
        }
        return result;
    }
    static double integerToDouble(int value) {
        double result = 0;
        if (value > 19.99d|| value < -19.99d) {
            System.err.println("Error! Out of value! - ");
        } else {
            result = (double) (value);
        }
        return result;
    }
    static int shortToInteger(short value) {
        int result = 0;
        if (value > 2147483647|| value < -2147483647) {
            System.err.println("Error! Out of value! - ");
        } else {
            result = (int) (value);
        }
        return result;
    }
    static short integerToShort(int value) {
        short result = 0;
        if (value > 32767|| value < -32767) {
            System.err.println("Error! Out of value! - ");
        } else {
            result = (short) (value);
        }
        return result;
    }
}


