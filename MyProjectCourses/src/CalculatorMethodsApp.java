public class CalculatorMethodsApp {
    // CLASS BEGIN

    public static void main(String[] args) {
        printDivider();
        printResult ( 1 + pow (2 , 3) * mul ( 3 , 4) - 5);
        printDivider();
    }

    static void printResult(int result) {
        int resultLength = String.valueOf(result).length();
        if (resultLength <= 8) {
            System.out.printf("result: %08d\n", result);
        } else {
            System.out.println("Error!");
        }
    }

    static void printDivider() {
        System.out.println("################");
    }

    static int add(int a, int b) {
        int r = a + b;
        return r;
    }

    static int sub(int a, int b) {
        int s = a - b;
        return s;
    }

    static int mul(int a, int b) {
        int m = a * b;
        return m;
    }

    static int div(int a, int b) {
        int d = a / b;
        return d;
    }

    static int pow(int v, int e) {
        int p = 1;
        for (int i = 1; i <= e; i++) {
            p = p * v;
        }
        return p;
    }
        // CLASS END


}
