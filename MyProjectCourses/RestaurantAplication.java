import java.util.Scanner;

public class RestaurantAplication {

	public static void main(String[] args) {
		//DATA
		final String CURENCY = "MDL";
		final String FOOD_1_NAME = "Pizza";
		final float FOOD_1_PRICE = 175.50f;
		final String FOOD_2_NAME = "Mamaliga";
		final float FOOD_2_PRICE = 50.00f;
		final String FOOD_3_NAME = "Salat";
		final float FOOD_3_PRICE = 30.00f;
		//PRESENTATION
		Scanner in = new Scanner(System.in);
		System.out.printf(
				"***********MENU**********\n" +
				"1. %-10s %7.2f %s \n"  +
				"2. %-10s %7.2f %s \n"+ 
				"3. %-10s %7.2f %s \n"+ 
				"*************************\n", 
				FOOD_1_NAME, FOOD_1_PRICE, CURENCY, 
				FOOD_2_NAME , FOOD_2_PRICE, CURENCY ,
				FOOD_3_NAME , FOOD_3_PRICE, CURENCY 
				
				);
		System.out.println("What will you chouse?");
		int option = in.nextInt();
		if (option == 1){
			System.out.println("You choice " + FOOD_1_NAME);
		}
		
	}

}
