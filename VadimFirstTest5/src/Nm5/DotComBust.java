package Nm5;
import Nm5.GameHelper;

import java.util.*;

public class DotComBust {
    private GameHelper helper = new GameHelper();
    private ArrayList<DotCom> dotComsList = new ArrayList<DotCom>();
    private int numOfGuesses = 0;

    private void setUpGame() {
        DotCom one = new DotCom();
        one.setName("Pets.com");
        DotCom two = new DotCom();
        two.setName("eToys.com");
        DotCom three = new DotCom();
        three.setName("Go2.com");
        dotComsList.add(one);
        dotComsList.add(two);
        dotComsList.add(three);

        System.out.println("Ваша задача уничтожить все три сайта.");
        System.out.println("Pets.com, eToys.com, Go2.com");
        System.out.println("Попробуйте ликвдировать их всех за минимальное количество попыток");

        for (DotCom dotComSet : dotComsList) {
            ArrayList<String> newLocation = helper.placeDotCom(3);
            dotComSet.setLocationCells(newLocation);
        }
    }

    private void startPlaying() {
        while (!dotComsList.isEmpty()) {
            String userGuess = helper.getUserInput("Сделай ход");
            checkUserGuess(userGuess);
        }
        finishGame();
    }

    private void checkUserGuess(String userGuess)
    {
        numOfGuesses++;
        String result = "Мимо";

        for (DotCom dotComToTest : dotComsList)
        {
            result = dotComToTest.checkYourself(userGuess);
            if (result.equals("Попал"))
            {
                break;
            }
            if (result.equals("Потопил"))
            {
                dotComsList.remove(dotComToTest);
                break;
            }
        }
        System.out.println(result);
    }

    private void finishGame() {
        System.out.println("Все сайты потоплены. Молодцы!");
        if (numOfGuesses <= 18) {
            System.out.println("Вам потребовалось всего" + numOfGuesses + " попыток");
            System.out.println("Вы слили их, до того как они слили тебя");
        }
        else
        {
            System.out.println("Вам потребовалось аж " + numOfGuesses + " попыток");
            System.out.println("Рыбы кормяться остатками ваших ходов");
        }
    }

    public static void main(String[] args) {
        DotComBust game = new DotComBust();
        game.setUpGame();
        game.startPlaying();
    }
}
