package main;


import project.Product;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class Application {

    @FunctionalInterface
    interface VATFunction extends Function<Product, Product> {

        @Override
        Product apply(Product product);


    }

    @FunctionalInterface
    interface IsSHPredicate extends Predicate<Product>
    {
        boolean test(Product product);
    }

    @FunctionalInterface
    interface IsHealthPredicate extends Predicate<Product> {
        boolean test(Product product);
    }

    @FunctionalInterface
    interface ConsolePrintConsumer extends Consumer<Product>
    {
        void accept(Product product);
    }
public static void main(String[]args){


    ArrayList<Product> productsArray = new ArrayList<>();
    productsArray.add(new Product("health", "Medic", 125f, 10));
    productsArray.add(new Product("second hand", "Foot", 825f, 11));
    productsArray.add(new Product("food", "Potato", 625f, 12));
    productsArray.add(new Product("clothes", "T-Shirt", 25f, 13));
    productsArray.add(new Product("health", "Medic2New", 175f, 14));

    ConsolePrintConsumer consumerInfo = product -> System.out.println(product);
    IsSHPredicate secondCategory = product -> !product.getCategory().equals("second hand");
    IsHealthPredicate healthCategory = product -> !product.getCategory().equals("health");
    VATFunction vatCalculate = product -> {
        product.setPrice((float)(product.getPrice() + product.getPrice() * 0.18));
        return product;
    };

    productsArray.stream()
            .filter(healthCategory)
            .filter(secondCategory)
            .map(vatCalculate)
            .forEach(consumerInfo);

}
}
