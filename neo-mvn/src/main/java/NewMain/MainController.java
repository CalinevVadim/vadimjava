package NewMain;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import space.NasaDataProvider;

import java.util.ArrayList;

public class MainController {

    @FXML
    private Button btn;
    @FXML
    private TextField txt1;
    @FXML
    private TextField txt2;
    @FXML
    private TextArea txtArea;

    @FXML
    private void click(ActionEvent event) {
        ArrayList<String> asteroidsButton = null;
        try {
            asteroidsButton = NasaDataProvider.getNeoAsteroids(txt1.getText(), txt2.getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < asteroidsButton.size()  ; i++) {
           String asteroidsSmallArray = txtArea.getText() + "\n" + asteroidsButton.get(i);

            txtArea.setText(asteroidsSmallArray);
        }
    }
}
