

import config.XMLConfigurationProvider;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
public class NasaProviderTest {
    @Test
    void checkNasaConnection() throws IOException {
        String findNasaKey = XMLConfigurationProvider.getValue("key", "src/test/resourses/nasa.xml");
        String findNasaUrl = XMLConfigurationProvider.getValue("url", "src/test/resourses/nasa.xml");
        URL url = new URL(findNasaUrl + findNasaKey);
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
        String inputLine;
        if ((inputLine = in.readLine())== null){
            fail("Failed connection to Nasa");
        }
    }
}