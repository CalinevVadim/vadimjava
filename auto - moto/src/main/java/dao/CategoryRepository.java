package dao;

import entities.Category;

import java.sql.*;
import java.util.Properties;

public class CategoryRepository {

    public Connection getConnection() throws SQLException {

        String url = "jdbc:postgresql://localhost/auto-moto";
        Properties props = new Properties();
        props.setProperty("user", "postgres");
        props.setProperty("password", "empire13");
        props.setProperty("ssl", "false");

        Connection conn = DriverManager.getConnection(url, props);

        return conn;

    }

    // INSERT
    public void insert(Category category) throws SQLException {
        PreparedStatement pst = getConnection().prepareStatement("INSERT INTO categories(id, name) VALUES (?, ?);");
        pst.setLong(1, category.getId());
        pst.setString(2, category.getName());
        pst.execute();
    }

    // SELECT
    public Category select(long id) throws SQLException {

        Statement st = getConnection().createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM categories WHERE id= " + id + ";");

        Category newCat = null;

        if (rs.next()) {
            newCat = new Category();
            newCat.setName(rs.getString("name"));
        }

        return newCat;

    }

    // UPDATE
    public void updateName(long id, String name) throws SQLException {
        getConnection().createStatement();
        PreparedStatement pst = getConnection().prepareStatement("UPDATE categories SET name=? WHERE id=" + id + ";");
        pst.setString(1, name);
        pst.executeUpdate();
    }

    // DELETE
    public void deleteCategory(long id) throws SQLException {
        getConnection().createStatement();
        PreparedStatement pst = getConnection().prepareStatement("DELETE FROM categories WHERE id=" + id + ";");
        pst.executeUpdate();

    }

}



