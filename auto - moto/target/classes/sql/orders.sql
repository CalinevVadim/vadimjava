-- Table: public.orders

-- DROP TABLE public.orders;

CREATE TABLE public.orders
(
    id bigint NOT NULL,
    quantity bigint NOT NULL,
    part_id bigint NOT NULL,
    CONSTRAINT orders_pkey PRIMARY KEY (id),
    CONSTRAINT orders_part_id_fkey FOREIGN KEY (part_id)
        REFERENCES public.parts (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.orders
    OWNER to postgres;