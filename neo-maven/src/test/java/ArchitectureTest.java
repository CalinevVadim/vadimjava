import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.fail;

class ArchitectureTest {
    @Test
    void checkBothCLIandGUI(){
        try {
            Class.forName("NewMain.CLIApplication");
            Class.forName("NewMain.GUIApplication");
        }catch (ClassNotFoundException e){
            fail("The Application doesn't have both GUI and CLI");
        }
    }
    @Test
    void checkMethodMain(){
        try {
            Class.forName("NewMain.CLIApplication").getMethod("main",String[].class);
            Class.forName("NewMain.GUIApplication").getMethod("main",String[].class);
        }catch (ClassNotFoundException e){
            fail("The Application doesn't have both GUI and CLI");
        } catch (NoSuchMethodException e) {
            fail("The Application class doesn't have ---main()");
        }
    }
    @Test
    void checkBothAsteroidandNasaDataProvider(){
        try {
            Class.forName("space.Asteroid");
            Class.forName("space.NasaDataProvider");
        }catch (ClassNotFoundException e){
            fail("The Application doesn't have both Asteroid and NasaDataProvider");
        }
    }
}


