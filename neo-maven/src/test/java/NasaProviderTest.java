import config.XMLConfigurationProvider;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.fail;

public class NasaProviderTest {
    @Test
    void checkNasaConnection() throws IOException {
        String findNasaKey = XMLConfigurationProvider.getValue("key", "nasa.xml");
        String findNasaUrl = XMLConfigurationProvider.getValue("url", "nasa.xml");
        URL url = new URL(findNasaUrl + "?start_date=2015-09-07&amp;end_date=2015-09-08&amp;api_key=" + findNasaKey);
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
        String inputLine;
        if ((inputLine = in.readLine())== null){
            fail("Failed connection to Nasa");
        }
    }
}