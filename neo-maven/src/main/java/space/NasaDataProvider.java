package space;

import java.net.*;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.time.LocalDate;
import java.util.Properties;

public class NasaDataProvider {
    private static Properties prop;
    private static String ACCESS_KEY;
    private static String NEO_ENDPOINT;

    static {
        prop = new Properties();
        try {
            prop.loadFromXML(new FileInputStream("C:\\Users\\Vladimir\\IdeaProjects\\neo-mvn\\src\\main\\resources\\nasa.xml"));
            ACCESS_KEY = prop.getProperty("nasa_key");
            ACCESS_KEY = prop.getProperty("nasa_url");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void getNeoAsteroids(String start, String end) throws Exception {
        //1 connect to NASA API
        URL nasa = new URL(NEO_ENDPOINT + "?start_date=" + start + "&end_date=" + end + "&api_key=" + ACCESS_KEY);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(nasa.openStream()));
        //2 read data
        String stringData = "";
        String inputLine;
        while ((inputLine = in.readLine()) != null)
//	     System.out.println(inputLine.replaceAll("}", "}\n"));
            stringData += inputLine;
        in.close();


        //3 Parse JSON
        JSONObject data = new JSONObject(stringData);
        int count = data.getInt("element_count");

        //4 Test some data
        System.out.println("Found " + count + " results");

        boolean isDangerous = data.getJSONObject("near_earth_objects")
                .getJSONArray(end).getJSONObject(0).getBoolean("is_potentially_hazardous_asteroid");
        System.out.println(isDangerous);


        for (LocalDate startTime = LocalDate.parse(start); !startTime.equals(LocalDate.parse(end)); ) {
            JSONArray arrayOfAll = data.getJSONObject("near_earth_objects")
                    .getJSONArray(startTime.toString());
            for (int i = 0; i < arrayOfAll.length(); i++) {
                JSONObject asteroid = arrayOfAll.getJSONObject(i);
                System.out.println(startTime + " - Date |" + asteroid.getString("name") + " - Name |" + asteroid.getJSONObject("estimated_diameter")
                        .getJSONObject("kilometers")
                        .getFloat("estimated_diameter_min") + " - Klm. Min. |"
                        + asteroid.getJSONObject("estimated_diameter")
                        .getJSONObject("kilometers")
                        .getFloat("estimated_diameter_max") + " - Klm. Max. |"
                        + asteroid.getJSONArray("close_approach_data")
                        .getJSONObject(0)
                        .getJSONObject("miss_distance")
                        .getString("kilometers") + " - Radius |"
                        + asteroid.getBoolean("is_potentially_hazardous_asteroid") + " - Is it dangerous? |");

            }
            startTime = startTime.plusDays(1);

        }
    }
}