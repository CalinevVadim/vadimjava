package config;



import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class XMLConfigurationProvider {

    public static String getValue(String keyName, String fileName){
        Properties prop = new Properties();
        try {
            prop.loadFromXML(new FileInputStream(fileName));
        } catch (IOException e) {
            System.out.println("Error! Can't find fileName");
            e.printStackTrace();

        }
        return prop.getProperty(keyName);
    }

}
//test
//XMLConfigurationProvider getValue("key", "nasa.xml")
//XMLConfigurationProvider getValue("url", "nasa.xml")