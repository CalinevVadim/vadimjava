package geometry;
import java.lang.Math.*;



public class Line {
    // properties
    public int startX;
    public int endX;
    public int startY;
    public int endY;


    // constructor
    public Line(int startX,int endX,int startY,int endY) {
        if (startX == endX || endY == startY) {
            System.err.println("Error!");
        } else {
            this.startY = startY;
            this.endY = endY;
            this.startX = startX;
            this.endX = endX;

        }
    }
    public void printCoords(){
System.out.printf("[ %d : %d] - - - -> [ %d : %d]", startX ,endX, startY ,endY );
    }
   public double length(){
       return (int)Math.sqrt(Math.pow((startX - startY), 2) +
               Math.pow((endX - endY), 2));
   }
}
