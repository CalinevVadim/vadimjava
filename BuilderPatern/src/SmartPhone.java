public class SmartPhone {
    private String brand;
    private int year;
    private String model;
    private int diagonal;
    private int memory;
    private int cores;
    private int battery;


    @Override
    public String toString() {
        return "SmartPhone{" +
                "brand='" + brand + '\'' +
                ", year=" + year +
                ", model='" + model + '\'' +
                ", diagonal=" + diagonal +
                ", memory=" + memory +
                ", cores=" + cores +
                ", battery=" + battery +
                '}';
    }



    public static class Builder {
        //Обязательные параметры
        private final  String brand;
        private final  int year;
        private final  String model;
        private final  int diagonal;

        //Необязательные параметры со значениями по умолчанию
        private  int memory;
        private  int cores;
        private  int battery;

        //Конструктор с обязательными параметрами
        public Builder(String brand, int year, String model, int diagonal) {
            this.brand = brand;
            this.year = year;
            this.model = model;
            this.diagonal = diagonal;
        }

        public Builder withMemory(int val) {
            memory = val;
            return this;
        }

        public Builder withCores(int val) {
            cores = val;
            return this;
        }

        public Builder withBattery(int val) {
            battery = val;
            return this;
        }

        public SmartPhone build() {
            return new SmartPhone(this);
        }
    }

    public SmartPhone(Builder builder) {
            brand = builder.brand;
            year = builder.year;
            model = builder.model;
            diagonal = builder.diagonal;
            memory = builder.memory;
            cores = builder.cores;
            battery = builder.battery;
        }

    }


