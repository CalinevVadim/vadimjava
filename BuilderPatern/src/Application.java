public class Application {
    public static void main(String[]args){
        SmartPhone x7 = new SmartPhone.Builder("Apple", 1999, "x7", 5)
                .withMemory(350)
                .withCores(34)
                .withBattery(345)
                .build();
        System.out.println(x7.toString());

    }
}
