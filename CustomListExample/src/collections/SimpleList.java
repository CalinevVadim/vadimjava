package collections;

public class SimpleList {

    // properties
    private int length;
    private Box first;

    // private data type
    private class Box {
        int value;
        Box next;

        public int getValue() {
            return value;
        }


        public Box(int value) {
            this.value = value;
        }

    }


    public int getLength() {
        return length;
    }

    // constructor
    public SimpleList() {
        length = 0;
        first = null;
    }

    // methods
    public void add(int value) {
        Box current = first;
        if (length == 0) {
            first = new Box(value);
        } else {
            for (int i = 0; i < length - 1; i++) {
                current = current.next;
            }
            current.next = new Box(value);
        }
        length++;
    }

    public void print() {
        Box print = first;
        if (length == 0) {
            System.out.println(first.getValue());
        }
        if (length > 0) {
            System.out.println(print.getValue());
            for (int i = 0; i < length - 1; i++) {
                print = print.next;
                System.out.println(print.getValue());
            }
        }
    }

    public void get(int index ) {
        Box print = first;
//        if (index == 1) {
//            System.out.println(first.getValue());
//        } else
//        if (index == 2) {
//            print = print.next;
//            System.out.println(print.getValue());
//        } else
//        if (index == 3) {
//            print = print.next;
//            System.out.println(print.getValue());
//        }
        for (int i = 0; i < index; i++) {
            print = print.next;
        }
        System.out.println(print.getValue());
    }

}


