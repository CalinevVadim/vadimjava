package Garage1;

public class Bus {
    static final int MAX_SEATS = 45;
    static final int MIN_SEATS = 0;
    private static int numberOfSeatsInBus = 0;
    private static int routeNumber = 77;

    public Bus() {
    }

    public static int getRouteNumber() {
        return routeNumber;
    }

    public static void setRouteNumber(int number) {
        if (number == 77 | number == 88 | number == 99) {
            routeNumber = number;
        } else {
            System.out.println("Error!");
        }

    }

    public static int getSeatsNumber() {
        return numberOfSeatsInBus;
    }

    public static void setSeatsNumber(int numberOfSeats) {
        if (numberOfSeats <= 45 & numberOfSeats >= 0) {
            numberOfSeatsInBus = numberOfSeats;
        } else {
            System.out.println("Error!");
        }

    }

    public static void showNumberOfSeats() {
        System.out.println("##################");
        System.out.printf("# %8d       # %n", numberOfSeatsInBus);
        System.out.println("##################");
    }

    public static int enterPassenger(int numOfPassengers) {
        if (numOfPassengers <= 2  &&  numberOfSeatsInBus <= (MAX_SEATS - numOfPassengers ) ){
            numberOfSeatsInBus += numOfPassengers;

        } else {
            System.out.println("Error!");

        }

        return numberOfSeatsInBus;
    }

    public static int exitPassenger(int numOfPassengers) {
        if (numOfPassengers <= 2 &&  numberOfSeatsInBus >= 1) {
            numberOfSeatsInBus -= numOfPassengers;
        } else {
            System.out.println("Error!");
        }

        return numberOfSeatsInBus;
    }

    public static class LEDPanel {
        public LEDPanel() {
        }

        public static void showRoute() {
            System.out.println("##################");
            System.out.printf("# %8d       # %n", Bus.routeNumber);
            System.out.println("##################");
        }
    }
    public static class Wheels {
        // Wheel numeric identifiers
        public final static int FRONT_LEFT = 1;
        public final static int FRONT_RIGHT = 2;
        public final static int BACK_LEFT_PRIMARY = 3;
        public final static int BACK_RIGHT_PRIMARY = 4;
        public final static int BACK_LEFT_SECONDARY = 5;
        public final static int BACK_RIGHT_SECONDARY = 6;
        // ???

        // wheel pressure values
        public static  double frontLeftPSI = 0.0;
        public   static double frontRightPSI = 0.0;
        public   static double backLeftPrimaryPSI = 0.0;
        public  static  double backRightPrimaryPSI = 0.0;
        public  static double backLeftSecondaryPSI = 0.0;
        public  static  double backRightSecondaryPSI = 0.0;

        public static double getWheelPSI(int whichWheel) {
            switch (whichWheel) {
                case 1:
                    return frontLeftPSI;

                case 2:
                    return frontRightPSI;

                case 3:
                    return backLeftPrimaryPSI;

                case 4:
                    return backRightPrimaryPSI;

                case 5:
                    return backLeftSecondaryPSI;

                case 6:
                    return backRightSecondaryPSI;

                default:
                    System.out.println("We don't have wheel witch such a number!");
                    return 0;
            }
        }



        public static  void setWheelPSI(double pressure, int whichWheel) {
            switch (whichWheel) {
                case 1:
                    if (pressure > 0.0 && pressure <= 120.0) {
                        System.out.println("Pressure is enough. On FRONT_LEFT it is - " + pressure);
                        frontLeftPSI =  pressure;
                    } else {
                        System.out.println("We can't do so!");
                    }
                    break;
                case 2:
                    if (pressure > 0.0 && pressure <= 120.0) {
                        System.out.println("Pressure is enough. On FRONT_RIGHT it is - " + pressure);
                        frontRightPSI = pressure;
                    } else {
                        System.out.println("We can't do so!");
                    }
                    break;
                case 3:
                    if (pressure > 0.0 && pressure <= 100.0) {
                        System.out.println("Pressure is enough. On BACK_LEFT_PRIMARY it is - " + pressure);
                        backLeftPrimaryPSI = pressure;
                    } else {
                        System.out.println("We can't do so!");
                    }
                    break;
                case 4:
                    if (pressure > 0.0 && pressure <= 100.0) {
                        System.out.println("Pressure is enough. On BACK_RIGHT_PRIMARY it is - " + pressure);
                        backRightPrimaryPSI = pressure;
                    } else {
                        System.out.println("We can't do so!");
                    }
                    break;
                case 5:
                    if (pressure > 0.0 && pressure <= 80.0) {
                        System.out.println("Pressure is enough. On BACK_LEFT_SECONDARY it is - " + pressure);
                        backLeftSecondaryPSI = pressure;
                    } else {
                        System.out.println("We can't do so!");
                    }
                    break;
                case 6:
                    if (pressure > 0.0 && pressure <= 80.0) {
                        System.out.println("Pressure is enough. On BACK_RIGHT_SECONDARY  it is - " + pressure);
                        backRightSecondaryPSI = pressure;
                    } else {
                        System.out.println("We can't do so!");
                    }
                    break;
                default:
                    System.out.println("We don't have wheel witch such a number!");
            }
        }
        public static boolean isGoodToGo(){
            if ( numberOfSeatsInBus >= 30 &&  frontLeftPSI <= 120.0 && frontRightPSI <= 120.0 &&
                    backLeftPrimaryPSI <= 100.0 &&  backRightPrimaryPSI <= 100.0 && backLeftSecondaryPSI <= 80.0 &&
                    backRightSecondaryPSI <= 80.0 ){
                return true; }
            if ( numberOfSeatsInBus <= 30 &&  frontLeftPSI <= 120.0 && frontRightPSI <= 120.0 &&
                    backLeftPrimaryPSI <= 100.0 &&  backRightPrimaryPSI <= 100.0){
                return true; } else
            { return false;
            }
        }
        }
    }


