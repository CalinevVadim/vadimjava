package City1;

import Garage1.Bus;
import Garage1.Bus.Wheels;



public class Application {

    public static void main(String[] args) {


            // 4 working wheels within parameters
            Bus.Wheels.setWheelPSI(115.0, Bus.Wheels.FRONT_LEFT);
            System.out.println(Bus.Wheels.frontLeftPSI);
            Bus.Wheels.setWheelPSI(115.0, Bus.Wheels.FRONT_RIGHT);
            System.out.println(Bus.Wheels.frontRightPSI);
            Bus.Wheels.setWheelPSI(100.0, Bus.Wheels.BACK_LEFT_PRIMARY);
            System.out.println(Bus.Wheels.backLeftPrimaryPSI);
            Bus.Wheels.setWheelPSI(100.0, Bus.Wheels.BACK_RIGHT_PRIMARY);
            System.out.println(Bus.Wheels.backRightPrimaryPSI);
            Bus.Wheels.setWheelPSI(0.0, Bus.Wheels.BACK_LEFT_SECONDARY);
            System.out.println(Bus.Wheels.backLeftSecondaryPSI);
            Bus.Wheels.setWheelPSI(0.0, Bus.Wheels.BACK_RIGHT_SECONDARY);
            System.out.println(Bus.Wheels.backRightSecondaryPSI);

            // Too many passengers
            Bus.setSeatsNumber(40);
            System.out.println(Bus.Wheels.isGoodToGo());
            // false

            // less passengers
            Bus.setSeatsNumber(29);
            System.out.println(Bus.Wheels.isGoodToGo());
            // true

            // pressurizing back wheels
            Bus.Wheels.setWheelPSI(80.0, Bus.Wheels.BACK_LEFT_SECONDARY);
            Bus.Wheels.setWheelPSI(80.0, Bus.Wheels.BACK_RIGHT_SECONDARY);


            // full bus
            Bus.setSeatsNumber(45);
            System.out.println(Bus.Wheels.isGoodToGo());
            // true


        }
    }
