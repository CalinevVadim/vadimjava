package Home;

public class Lift {

    // Lift's memory
    final static double MAX_WEIGHT = 1000.0; // kg
    final static byte MAX_LEVEL = 11;
    final static byte MIN_LEVEL = 0;
    private static boolean isOperational = true;
    private static boolean isMoving = false;
    private static boolean isClosed = true;
    private static byte level = 0;
    private static double weight = 0;

    public static byte getLevel() {
        return level;
    }

    public boolean isOperational() {
        return isOperational;
    }

    public boolean isMoving() {
        return isMoving;
    }

    public boolean isClosed() {
        return isClosed;
    }

    private static void setLevel(int level) {
        if (level < MIN_LEVEL || level > MAX_LEVEL) {
            System.err.println("Level value out of bounds!");
            return;
        }
        Lift.level = (byte) level;
    }

    private boolean setIsMoving(boolean isMoving) {
        if (weight <= MAX_WEIGHT) {
            return true;
        } else {
            return false;
        }
    }
        public static void printLiftMap() {
            for (int number = 11; number >= MIN_LEVEL; number--) {
                if (number == 6) {
                    System.out.printf("%2d |   [    ]   | \n", number);
                } else {
                    System.out.printf("%2d |            | \n", number);
                    if (number == 5) {
                        System.out.printf("%2d |     v      | \n", number);

                    }
                }
            }
        }
        // Lift's actions
        // ...
    }
