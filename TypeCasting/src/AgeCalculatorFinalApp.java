import java.util.Scanner;

public class AgeCalculatorFinalApp {
    public static void main(String[] args) {
        final double THIS_YEAR = 2019;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter your year of birth");
        String year1 = in.next();
        String year2 = in.next();
        String year3  = in.next();
        String year4 = in.next();
        String yearOfBirth = year1 + year2 + year3 + year4;
        int age =(int) THIS_YEAR - Integer.parseInt(yearOfBirth);
        System.out.println("This mean your age - " + age + " years");

    }
}
