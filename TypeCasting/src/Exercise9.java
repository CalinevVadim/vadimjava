public class Exercise9 {
    public static void main(String[] args) {
        int a = 10;
        int b = 100;
        int c = 1000;

        int result = ++a + ++b + ++c; // Вначале присваевает значение, потом использует уже (11 вместо 10) его для вычисления.

        System.out.println( "( " + a + "," + b + "," + c + " ) -> " + result );
    }
}
