public class Exercise2 {
    public static void main (String[]args){
        final double kmInMile = 1.60934;
        int distanceBetweenCitiesMiles = 200;
        int carSpeedKm = 160; // per hour
        double distanceBetweenCitiesKm = (double) distanceBetweenCitiesMiles  * kmInMile ;//convert miles in km;
        int timeTakenHours = (int) distanceBetweenCitiesKm / carSpeedKm;
        int timeTakenMinutes = (int) distanceBetweenCitiesKm % carSpeedKm;
        System.out.print("The road takes " + timeTakenHours + " h ");
        System.out.println(timeTakenMinutes + " min");



    }
}
