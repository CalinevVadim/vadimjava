public class Exercise6 {
    public static void main(String[] args) {
        int moneyOne = 100;
        int moneyTwo = 200;
        int moneyThree = 300;

        int moneyAccum = 0;

        int expenses = 45;

        System.out.println("\u001B[31m+\u001B[0m\t" + moneyOne + "\u001B[34m MDL:\u001B[0m\n" +
                "\u001B[31m+\u001B[0m\t" + moneyTwo + "\n" +
                "\u001B[31m+\u001B[0m\t" + moneyThree + "\n" +
                "\u001B[31m-\u001B[0m\t" + expenses + "\n" +
                "\u001B[31m\t-----------\u001B[0m");
        moneyAccum += moneyOne;
        moneyAccum += moneyTwo;
        moneyAccum += moneyThree;

        moneyAccum -= expenses;
        System.out.println("\t" + moneyAccum);
    }
}