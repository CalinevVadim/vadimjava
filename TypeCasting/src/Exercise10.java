public class Exercise10 {
    public static void main(String[] args) {
        int x = 10;
        int result = ++x + ++x + ++x; //11 + 12 + 13

        System.out.println( "( " + x + " ) -> " + result );
    }
}
