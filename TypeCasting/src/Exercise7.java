public class Exercise7 {
    public static void main(String[] args) {
        // # DATA - CONSTANTS
        final int YEAR_DAYS = 365;
        final int MONTH_DAYS = 30;
        final int WEEK_DAYS = 7;

        // # DATA - variables
        int periodDays = 1234;
        int toYears;
        int toMonth;
        int toWeeks;

        //CALCULATION
        int leftDays = 1234;
        toYears = periodDays / YEAR_DAYS;
        leftDays -= toYears * YEAR_DAYS;

        toMonth = leftDays / MONTH_DAYS;
        leftDays -= toMonth * MONTH_DAYS;

        toWeeks = leftDays / WEEK_DAYS;
        leftDays -= toWeeks * WEEK_DAYS;

        // # PRINT RESULTS
        System.out.println(periodDays + " days is : " + toYears + " years, " +
                toMonth + " months, " + toWeeks + " weeks and " + leftDays + " days");
    }
}